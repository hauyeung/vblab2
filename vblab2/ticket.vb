﻿Public Class ticket
    Const percentkeep As Double = 0.2
    Function grossrevenue(ByVal priceperadultticket As Double, ByVal numadultticketssold As Integer, ByVal priceperchildticket As Double, ByVal numchildticketssold As Integer) As Double
        Return priceperadultticket * numadultticketssold + numchildticketssold * priceperchildticket
    End Function

    Function netrevenue(ByVal priceperadultticket As Double, ByVal numadultticketssold As Integer, ByVal priceperchildticket As Double, ByVal numchildticketssold As Integer) As Double
        Const percentkeep As Double = 0.2
        Return percentkeep * (priceperadultticket * numadultticketssold + numchildticketssold * priceperchildticket)
    End Function

    Function grossadultrevenue(ByVal priceperadultticket As Double, ByVal numadultticketssold As Integer) As Double
        Return priceperadultticket * numadultticketssold
    End Function

    Function grosschildrevenue(ByVal priceperchildticket As Double, ByVal numchildticketssold As Integer) As Double
        Return priceperchildticket * numchildticketssold
    End Function

End Class
