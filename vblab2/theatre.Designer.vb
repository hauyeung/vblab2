﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class theatre
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblAdultTicketsSold = New System.Windows.Forms.Label()
        Me.lblAdultPricePerTicket = New System.Windows.Forms.Label()
        Me.txtAdultTicketsSold = New System.Windows.Forms.TextBox()
        Me.txtPricePerAdultTicket = New System.Windows.Forms.TextBox()
        Me.btnCalculateTicketRevenue = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.txtTotalGrossRevenueAdultTicket = New System.Windows.Forms.TextBox()
        Me.txtGrossChildTicketSales = New System.Windows.Forms.TextBox()
        Me.txtGrossAdultTicketSales = New System.Windows.Forms.TextBox()
        Me.lblGrossRevenue = New System.Windows.Forms.Label()
        Me.lblGrossChildTicketSales = New System.Windows.Forms.Label()
        Me.lblGrossAdultTicketSales = New System.Windows.Forms.Label()
        Me.txtNetTotalRevenue = New System.Windows.Forms.TextBox()
        Me.txtNetChildTicketSales = New System.Windows.Forms.TextBox()
        Me.txtNetAdultTicketSales = New System.Windows.Forms.TextBox()
        Me.lblNetRevenue = New System.Windows.Forms.Label()
        Me.lblNetChildTicketSales = New System.Windows.Forms.Label()
        Me.lblNetAdultTicketSales = New System.Windows.Forms.Label()
        Me.txtPricePerChildTicket = New System.Windows.Forms.TextBox()
        Me.txtChildTicketsSold = New System.Windows.Forms.TextBox()
        Me.lblPricePerChildTicket = New System.Windows.Forms.Label()
        Me.lblChildTicketsSold = New System.Windows.Forms.Label()
        Me.grpAdultTicketSales = New System.Windows.Forms.GroupBox()
        Me.grpChildTicketSales = New System.Windows.Forms.GroupBox()
        Me.grpGrossTicketRevenue = New System.Windows.Forms.GroupBox()
        Me.grpNetTicketRevenue = New System.Windows.Forms.GroupBox()
        Me.grpAdultTicketSales.SuspendLayout()
        Me.grpChildTicketSales.SuspendLayout()
        Me.grpGrossTicketRevenue.SuspendLayout()
        Me.grpNetTicketRevenue.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblAdultTicketsSold
        '
        Me.lblAdultTicketsSold.AutoSize = True
        Me.lblAdultTicketsSold.Location = New System.Drawing.Point(25, 53)
        Me.lblAdultTicketsSold.Name = "lblAdultTicketsSold"
        Me.lblAdultTicketsSold.Size = New System.Drawing.Size(66, 13)
        Me.lblAdultTicketsSold.TabIndex = 4
        Me.lblAdultTicketsSold.Text = "Tickets Sold"
        '
        'lblAdultPricePerTicket
        '
        Me.lblAdultPricePerTicket.AutoSize = True
        Me.lblAdultPricePerTicket.Location = New System.Drawing.Point(25, 25)
        Me.lblAdultPricePerTicket.Name = "lblAdultPricePerTicket"
        Me.lblAdultPricePerTicket.Size = New System.Drawing.Size(83, 13)
        Me.lblAdultPricePerTicket.TabIndex = 3
        Me.lblAdultPricePerTicket.Text = "Price Per Ticket"
        '
        'txtAdultTicketsSold
        '
        Me.txtAdultTicketsSold.Location = New System.Drawing.Point(114, 46)
        Me.txtAdultTicketsSold.Name = "txtAdultTicketsSold"
        Me.txtAdultTicketsSold.Size = New System.Drawing.Size(128, 20)
        Me.txtAdultTicketsSold.TabIndex = 2
        '
        'txtPricePerAdultTicket
        '
        Me.txtPricePerAdultTicket.Location = New System.Drawing.Point(114, 19)
        Me.txtPricePerAdultTicket.Name = "txtPricePerAdultTicket"
        Me.txtPricePerAdultTicket.Size = New System.Drawing.Size(128, 20)
        Me.txtPricePerAdultTicket.TabIndex = 1
        '
        'btnCalculateTicketRevenue
        '
        Me.btnCalculateTicketRevenue.Location = New System.Drawing.Point(78, 302)
        Me.btnCalculateTicketRevenue.Name = "btnCalculateTicketRevenue"
        Me.btnCalculateTicketRevenue.Size = New System.Drawing.Size(148, 23)
        Me.btnCalculateTicketRevenue.TabIndex = 2
        Me.btnCalculateTicketRevenue.Text = "Calculate Ticket Revenue"
        Me.btnCalculateTicketRevenue.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(264, 303)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 3
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(396, 303)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtTotalGrossRevenueAdultTicket
        '
        Me.txtTotalGrossRevenueAdultTicket.Enabled = False
        Me.txtTotalGrossRevenueAdultTicket.Location = New System.Drawing.Point(142, 89)
        Me.txtTotalGrossRevenueAdultTicket.Name = "txtTotalGrossRevenueAdultTicket"
        Me.txtTotalGrossRevenueAdultTicket.Size = New System.Drawing.Size(100, 20)
        Me.txtTotalGrossRevenueAdultTicket.TabIndex = 6
        '
        'txtGrossChildTicketSales
        '
        Me.txtGrossChildTicketSales.Enabled = False
        Me.txtGrossChildTicketSales.Location = New System.Drawing.Point(142, 63)
        Me.txtGrossChildTicketSales.Name = "txtGrossChildTicketSales"
        Me.txtGrossChildTicketSales.Size = New System.Drawing.Size(100, 20)
        Me.txtGrossChildTicketSales.TabIndex = 5
        '
        'txtGrossAdultTicketSales
        '
        Me.txtGrossAdultTicketSales.Enabled = False
        Me.txtGrossAdultTicketSales.Location = New System.Drawing.Point(142, 38)
        Me.txtGrossAdultTicketSales.Name = "txtGrossAdultTicketSales"
        Me.txtGrossAdultTicketSales.Size = New System.Drawing.Size(100, 20)
        Me.txtGrossAdultTicketSales.TabIndex = 4
        '
        'lblGrossRevenue
        '
        Me.lblGrossRevenue.AutoSize = True
        Me.lblGrossRevenue.Location = New System.Drawing.Point(25, 86)
        Me.lblGrossRevenue.Name = "lblGrossRevenue"
        Me.lblGrossRevenue.Size = New System.Drawing.Size(185, 13)
        Me.lblGrossRevenue.TabIndex = 3
        Me.lblGrossRevenue.Text = "Total Gross Revenue for Ticket Sales"
        Me.lblGrossRevenue.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGrossChildTicketSales
        '
        Me.lblGrossChildTicketSales.AutoSize = True
        Me.lblGrossChildTicketSales.Location = New System.Drawing.Point(25, 63)
        Me.lblGrossChildTicketSales.Name = "lblGrossChildTicketSales"
        Me.lblGrossChildTicketSales.Size = New System.Drawing.Size(92, 13)
        Me.lblGrossChildTicketSales.TabIndex = 2
        Me.lblGrossChildTicketSales.Text = "Child Ticket Sales"
        Me.lblGrossChildTicketSales.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGrossAdultTicketSales
        '
        Me.lblGrossAdultTicketSales.AutoSize = True
        Me.lblGrossAdultTicketSales.Location = New System.Drawing.Point(25, 41)
        Me.lblGrossAdultTicketSales.Name = "lblGrossAdultTicketSales"
        Me.lblGrossAdultTicketSales.Size = New System.Drawing.Size(93, 13)
        Me.lblGrossAdultTicketSales.TabIndex = 1
        Me.lblGrossAdultTicketSales.Text = "Adult Ticket Sales"
        Me.lblGrossAdultTicketSales.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtNetTotalRevenue
        '
        Me.txtNetTotalRevenue.Enabled = False
        Me.txtNetTotalRevenue.Location = New System.Drawing.Point(151, 90)
        Me.txtNetTotalRevenue.Name = "txtNetTotalRevenue"
        Me.txtNetTotalRevenue.Size = New System.Drawing.Size(100, 20)
        Me.txtNetTotalRevenue.TabIndex = 6
        '
        'txtNetChildTicketSales
        '
        Me.txtNetChildTicketSales.Enabled = False
        Me.txtNetChildTicketSales.Location = New System.Drawing.Point(151, 63)
        Me.txtNetChildTicketSales.Name = "txtNetChildTicketSales"
        Me.txtNetChildTicketSales.Size = New System.Drawing.Size(100, 20)
        Me.txtNetChildTicketSales.TabIndex = 5
        '
        'txtNetAdultTicketSales
        '
        Me.txtNetAdultTicketSales.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me.txtNetAdultTicketSales.Enabled = False
        Me.txtNetAdultTicketSales.Location = New System.Drawing.Point(151, 37)
        Me.txtNetAdultTicketSales.Name = "txtNetAdultTicketSales"
        Me.txtNetAdultTicketSales.Size = New System.Drawing.Size(100, 20)
        Me.txtNetAdultTicketSales.TabIndex = 4
        '
        'lblNetRevenue
        '
        Me.lblNetRevenue.AutoSize = True
        Me.lblNetRevenue.Location = New System.Drawing.Point(36, 90)
        Me.lblNetRevenue.Name = "lblNetRevenue"
        Me.lblNetRevenue.Size = New System.Drawing.Size(175, 13)
        Me.lblNetRevenue.TabIndex = 3
        Me.lblNetRevenue.Text = "Total Net Revenue for Ticket Sales"
        Me.lblNetRevenue.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNetChildTicketSales
        '
        Me.lblNetChildTicketSales.AutoSize = True
        Me.lblNetChildTicketSales.Location = New System.Drawing.Point(36, 64)
        Me.lblNetChildTicketSales.Name = "lblNetChildTicketSales"
        Me.lblNetChildTicketSales.Size = New System.Drawing.Size(92, 13)
        Me.lblNetChildTicketSales.TabIndex = 2
        Me.lblNetChildTicketSales.Text = "Child Ticket Sales"
        Me.lblNetChildTicketSales.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNetAdultTicketSales
        '
        Me.lblNetAdultTicketSales.AutoSize = True
        Me.lblNetAdultTicketSales.Location = New System.Drawing.Point(36, 42)
        Me.lblNetAdultTicketSales.Name = "lblNetAdultTicketSales"
        Me.lblNetAdultTicketSales.Size = New System.Drawing.Size(93, 13)
        Me.lblNetAdultTicketSales.TabIndex = 1
        Me.lblNetAdultTicketSales.Text = "Adult Ticket Sales"
        Me.lblNetAdultTicketSales.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtPricePerChildTicket
        '
        Me.txtPricePerChildTicket.Location = New System.Drawing.Point(104, 25)
        Me.txtPricePerChildTicket.Name = "txtPricePerChildTicket"
        Me.txtPricePerChildTicket.Size = New System.Drawing.Size(125, 20)
        Me.txtPricePerChildTicket.TabIndex = 1
        '
        'txtChildTicketsSold
        '
        Me.txtChildTicketsSold.Location = New System.Drawing.Point(104, 52)
        Me.txtChildTicketsSold.Name = "txtChildTicketsSold"
        Me.txtChildTicketsSold.Size = New System.Drawing.Size(125, 20)
        Me.txtChildTicketsSold.TabIndex = 2
        '
        'lblPricePerChildTicket
        '
        Me.lblPricePerChildTicket.AutoSize = True
        Me.lblPricePerChildTicket.Location = New System.Drawing.Point(11, 25)
        Me.lblPricePerChildTicket.Name = "lblPricePerChildTicket"
        Me.lblPricePerChildTicket.Size = New System.Drawing.Size(83, 13)
        Me.lblPricePerChildTicket.TabIndex = 3
        Me.lblPricePerChildTicket.Text = "Price Per Ticket"
        '
        'lblChildTicketsSold
        '
        Me.lblChildTicketsSold.AutoSize = True
        Me.lblChildTicketsSold.Location = New System.Drawing.Point(11, 55)
        Me.lblChildTicketsSold.Name = "lblChildTicketsSold"
        Me.lblChildTicketsSold.Size = New System.Drawing.Size(66, 13)
        Me.lblChildTicketsSold.TabIndex = 4
        Me.lblChildTicketsSold.Text = "Tickets Sold"
        '
        'grpAdultTicketSales
        '
        Me.grpAdultTicketSales.Controls.Add(Me.lblAdultTicketsSold)
        Me.grpAdultTicketSales.Controls.Add(Me.txtPricePerAdultTicket)
        Me.grpAdultTicketSales.Controls.Add(Me.lblAdultPricePerTicket)
        Me.grpAdultTicketSales.Controls.Add(Me.txtAdultTicketsSold)
        Me.grpAdultTicketSales.Location = New System.Drawing.Point(19, 24)
        Me.grpAdultTicketSales.Name = "grpAdultTicketSales"
        Me.grpAdultTicketSales.Size = New System.Drawing.Size(268, 100)
        Me.grpAdultTicketSales.TabIndex = 7
        Me.grpAdultTicketSales.TabStop = False
        Me.grpAdultTicketSales.Text = "Adult Ticket Sales"
        '
        'grpChildTicketSales
        '
        Me.grpChildTicketSales.Controls.Add(Me.lblChildTicketsSold)
        Me.grpChildTicketSales.Controls.Add(Me.txtPricePerChildTicket)
        Me.grpChildTicketSales.Controls.Add(Me.lblPricePerChildTicket)
        Me.grpChildTicketSales.Controls.Add(Me.txtChildTicketsSold)
        Me.grpChildTicketSales.Location = New System.Drawing.Point(304, 24)
        Me.grpChildTicketSales.Name = "grpChildTicketSales"
        Me.grpChildTicketSales.Size = New System.Drawing.Size(254, 100)
        Me.grpChildTicketSales.TabIndex = 8
        Me.grpChildTicketSales.TabStop = False
        Me.grpChildTicketSales.Text = "Child Ticket Sales"
        '
        'grpGrossTicketRevenue
        '
        Me.grpGrossTicketRevenue.Controls.Add(Me.txtTotalGrossRevenueAdultTicket)
        Me.grpGrossTicketRevenue.Controls.Add(Me.txtGrossAdultTicketSales)
        Me.grpGrossTicketRevenue.Controls.Add(Me.txtGrossChildTicketSales)
        Me.grpGrossTicketRevenue.Controls.Add(Me.lblGrossAdultTicketSales)
        Me.grpGrossTicketRevenue.Controls.Add(Me.lblGrossChildTicketSales)
        Me.grpGrossTicketRevenue.Controls.Add(Me.lblGrossRevenue)
        Me.grpGrossTicketRevenue.Location = New System.Drawing.Point(19, 151)
        Me.grpGrossTicketRevenue.Name = "grpGrossTicketRevenue"
        Me.grpGrossTicketRevenue.Size = New System.Drawing.Size(261, 130)
        Me.grpGrossTicketRevenue.TabIndex = 9
        Me.grpGrossTicketRevenue.TabStop = False
        Me.grpGrossTicketRevenue.Text = "Gross Ticket Revenue"
        '
        'grpNetTicketRevenue
        '
        Me.grpNetTicketRevenue.Controls.Add(Me.txtNetTotalRevenue)
        Me.grpNetTicketRevenue.Controls.Add(Me.txtNetAdultTicketSales)
        Me.grpNetTicketRevenue.Controls.Add(Me.txtNetChildTicketSales)
        Me.grpNetTicketRevenue.Controls.Add(Me.lblNetAdultTicketSales)
        Me.grpNetTicketRevenue.Controls.Add(Me.lblNetChildTicketSales)
        Me.grpNetTicketRevenue.Controls.Add(Me.lblNetRevenue)
        Me.grpNetTicketRevenue.Location = New System.Drawing.Point(305, 151)
        Me.grpNetTicketRevenue.Name = "grpNetTicketRevenue"
        Me.grpNetTicketRevenue.Size = New System.Drawing.Size(258, 130)
        Me.grpNetTicketRevenue.TabIndex = 10
        Me.grpNetTicketRevenue.TabStop = False
        Me.grpNetTicketRevenue.Text = "Net Ticket Revenue"
        '
        'theatre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(575, 352)
        Me.Controls.Add(Me.grpNetTicketRevenue)
        Me.Controls.Add(Me.grpGrossTicketRevenue)
        Me.Controls.Add(Me.grpChildTicketSales)
        Me.Controls.Add(Me.grpAdultTicketSales)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCalculateTicketRevenue)
        Me.Name = "theatre"
        Me.Text = "Theatres Revenue"
        Me.grpAdultTicketSales.ResumeLayout(False)
        Me.grpAdultTicketSales.PerformLayout()
        Me.grpChildTicketSales.ResumeLayout(False)
        Me.grpChildTicketSales.PerformLayout()
        Me.grpGrossTicketRevenue.ResumeLayout(False)
        Me.grpGrossTicketRevenue.PerformLayout()
        Me.grpNetTicketRevenue.ResumeLayout(False)
        Me.grpNetTicketRevenue.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCalculateTicketRevenue As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtAdultTicketsSold As System.Windows.Forms.TextBox
    Friend WithEvents txtPricePerAdultTicket As System.Windows.Forms.TextBox
    Friend WithEvents lblAdultTicketsSold As System.Windows.Forms.Label
    Friend WithEvents lblAdultPricePerTicket As System.Windows.Forms.Label
    Friend WithEvents lblGrossChildTicketSales As System.Windows.Forms.Label
    Friend WithEvents lblGrossAdultTicketSales As System.Windows.Forms.Label
    Friend WithEvents lblNetChildTicketSales As System.Windows.Forms.Label
    Friend WithEvents lblNetAdultTicketSales As System.Windows.Forms.Label
    Friend WithEvents lblGrossRevenue As System.Windows.Forms.Label
    Friend WithEvents lblNetRevenue As System.Windows.Forms.Label
    Friend WithEvents txtTotalGrossRevenueAdultTicket As System.Windows.Forms.TextBox
    Friend WithEvents txtGrossChildTicketSales As System.Windows.Forms.TextBox
    Friend WithEvents txtGrossAdultTicketSales As System.Windows.Forms.TextBox
    Friend WithEvents txtNetTotalRevenue As System.Windows.Forms.TextBox
    Friend WithEvents txtNetChildTicketSales As System.Windows.Forms.TextBox
    Friend WithEvents txtNetAdultTicketSales As System.Windows.Forms.TextBox
    Friend WithEvents txtPricePerChildTicket As System.Windows.Forms.TextBox
    Friend WithEvents txtChildTicketsSold As System.Windows.Forms.TextBox
    Friend WithEvents lblPricePerChildTicket As System.Windows.Forms.Label
    Friend WithEvents lblChildTicketsSold As System.Windows.Forms.Label
    Friend WithEvents grpAdultTicketSales As System.Windows.Forms.GroupBox
    Friend WithEvents grpChildTicketSales As System.Windows.Forms.GroupBox
    Friend WithEvents grpGrossTicketRevenue As System.Windows.Forms.GroupBox
    Friend WithEvents grpNetTicketRevenue As System.Windows.Forms.GroupBox

End Class
