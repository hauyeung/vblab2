﻿Public Class theatre

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblGrossRevenue.Text = "Total Gross Revenue" + vbCrLf + "for Ticket Sales"
        lblNetRevenue.Text = "Total Net Revenue" + vbCrLf + " for Ticket Sales"
    End Sub


    Private Sub btnCalculateTicketRevenue_Click(sender As Object, e As EventArgs) Handles btnCalculateTicketRevenue.Click
        Const percentkeep As Double = 0.2
        Dim ticket As New ticket
        Dim priceperadultticket As Double
        Dim priceperchildticket As Double
        Dim numadultticketssold As Integer
        Dim numchildticketssold As Integer
        Dim grosstotalrevenue As Double
        Dim nettotalrevenue As Double
        Dim grossadultrevenue As Double
        Dim netadultrevenue As Double
        Dim grosschildrevenue As Double
        Dim netchildrevenue As Double
        Dim priceperadultsucceecd As Integer = Double.TryParse(txtPricePerAdultTicket.Text, priceperadultticket)
        Dim priceperchildsucceed As Integer = Double.TryParse(txtPricePerChildTicket.Text, priceperchildticket)
        Dim numadultssucceed As Integer = Integer.TryParse(txtAdultTicketsSold.Text, numadultticketssold)
        Dim numchildsucceed As Integer = Integer.TryParse(txtChildTicketsSold.Text, numchildticketssold)
        If priceperadultsucceecd = 0 Or priceperadultticket < 0 Then
            MessageBox.Show("Please enter a number")
        End If
        If priceperchildsucceed = 0 Or priceperchildticket < 0 Then
            MessageBox.Show("Please enter a number")
        End If
        If numadultssucceed = 0 Or numadultticketssold < 0 Then
            MessageBox.Show("Please enter a number")
        End If
        If numchildsucceed = 0 Or numchildticketssold < 0 Then
            MessageBox.Show("Please enter a number")
        End If
        grosstotalrevenue = ticket.grossrevenue(priceperadultticket, numadultticketssold, priceperchildticket, numchildticketssold)
        nettotalrevenue = ticket.netrevenue(priceperadultticket, numadultticketssold, priceperadultticket, numadultticketssold)
        grossadultrevenue = ticket.grossadultrevenue(priceperadultticket, numadultticketssold)
        netadultrevenue = percentkeep * grossadultrevenue
        grosschildrevenue = ticket.grosschildrevenue(priceperchildticket, numchildticketssold)
        netchildrevenue = percentkeep * grosschildrevenue
        nettotalrevenue = netadultrevenue + netchildrevenue
        txtGrossAdultTicketSales.Text = "$" + Format(grossadultrevenue, "0.00").ToString()
        txtNetAdultTicketSales.Text = "$" + Format(netadultrevenue, "0.00").ToString()
        txtGrossChildTicketSales.Text = "$" + Format(grosschildrevenue, "0.00").ToString()
        txtNetChildTicketSales.Text = "$" + Format(netchildrevenue, "0.00").ToString()
        txtTotalGrossRevenueAdultTicket.Text = "$" + Format(grosstotalrevenue, "0.00").ToString()
        txtNetTotalRevenue.Text = "$" + Format(nettotalrevenue, "0.00").ToString()




    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtGrossAdultTicketSales.Text = ""
        txtNetAdultTicketSales.Text = ""
        txtGrossChildTicketSales.Text = ""
        txtNetChildTicketSales.Text = ""
        txtTotalGrossRevenueAdultTicket.Text = ""
        txtNetTotalRevenue.Text = ""
        txtPricePerAdultTicket.Text = ""
        txtPricePerChildTicket.Text = ""
        txtAdultTicketsSold.Text = ""
        txtChildTicketsSold.Text = ""        
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub
End Class
